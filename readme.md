本文件將說明各個檔案在專案中所扮演的角色

# Shader
## Transition.shader
這是一個會吃一張黑白的灰階素材當作Source，讓畫面用指定顏色從黑色的地方開始慢慢填滿到白色的Shader。  
也可以拿來用作畫面四角的血汙，相當便利。  
用於Resonance專案中演出的轉場。

## MaskFill.shader
這是一個會在指定區域用指定顏色的線段隨機填滿直到整塊變成同一個顏色的Shader，  
使用在標題畫面的選項中。

# Script
## DashVFX
這是在Resonance專案中，用來Render 2D遊戲中常見的殘影效果的東西。  
DashEffectVFX.cs是殘影本身，包含了逐漸透明和讓顯示順序顯示在正確的地方的部分。  
DashRenderer.cs則是創造殘影的Component。  
具體產生的殘影特效則是在Unity設定Prefab產生。

## TimeUnitCooldown.cs
Resonance中處理時間還剩下多久的Cooldown Class，  
因為太常用到類似這種「查看跟開始時差了多久」、「還有多久要結束」的行為而寫的。  
TimeUnit是Resonance為了和音樂溝通所設計的「音樂術語-時間對應結構」。