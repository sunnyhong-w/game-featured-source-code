Shader "SIGHT./UI/MaskFill"
{
    Properties
    {

        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)

        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0

        _LineColor ("Line Color", Color) = (1,1,1,1)
        _FadeRatio ("Fade Ratio", float) = 0
        _WHRatio ("Width Height Ratio", float) = 0
        _MaxWidth ("MaxWidth", float) = 0.3
		_MinWidth ("MinWidth", float) = 0.1
		_LineCount ("LineCount", int) = 5
        _Speed ("Speed", float) = 1
        _Offset ("Offset", float) = 1

        _ColorStepRatio ("Color Step Ratio", float) = 0
        _ColorStep ("Color Step", Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]

        Pass
        {
            Name "Default"
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            #include "UnityCG.cginc"
            #include "UnityUI.cginc"

            #pragma multi_compile __ UNITY_UI_CLIP_RECT
            #pragma multi_compile __ UNITY_UI_ALPHACLIP

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            fixed4 _Color;
            fixed4 _TextureSampleAdd;
            float4 _ClipRect;

            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

                OUT.texcoord = v.texcoord;

                OUT.color = v.color * _Color;
                return OUT;
            }

            sampler2D _MainTex;
            fixed4 _LineColor;
            float _FadeRatio;
            float _WHRatio;

            float _MaxWidth;
			float _MinWidth;
            float _Speed;
			int _LineCount;
            float _Offset;

            float _ColorStepRatio;
            fixed4 _ColorStep;

            float rand (in float x) {
                return frac(sin(x)*1e4);
            }

            float plotMask(float2 vertex, float dx, float w, float deg){
                return step(smoothstep(deg - w + dx, deg + dx, vertex.y) - smoothstep(deg + dx, deg + w + dx, vertex.y), 0);
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                /*
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;

                #ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif

                #ifdef UNITY_UI_ALPHACLIP
                clip (color.a - 0.001);
                #endif
                */
                half4 color;
                float mask = 0;

                _FadeRatio = clamp(_FadeRatio, 0, 1);

                color = _LineColor * _Color;

                float dstep = _ColorStepRatio * 2;
                dstep = dstep > 1 ? dstep - 1 : dstep;
                float stepmask = 1 - (_ColorStepRatio <= 0.5 ? step(dstep, IN.texcoord.x) : step(1 - dstep, 1 - IN.texcoord.x));
                color = color * (1 - stepmask) + _ColorStep * stepmask;
                
                if(_FadeRatio == 1) //Maybe wrong?
                    return color;

                for(int i = 0; i < _LineCount; i++)
				{
					float dx = frac(rand(_Time.x + i + _Offset) * _Speed) * _WHRatio;
					float w = frac(rand(_Time.x + 1 + i + _Offset) * _Speed) * (_MaxWidth - _MinWidth) + _MinWidth;
					mask +=  1 - plotMask(IN.texcoord, -1 * dx, w, IN.texcoord.x * _WHRatio);
				}

                float fademask = _FadeRatio < 1 ? 0 : 1;
                fixed4 white = fixed4(1,1,1,1);

                color = (1 - fademask) * (mask * _FadeRatio * _LineColor * _Color);

                return color;
            }
        ENDCG
        }
    }
}
