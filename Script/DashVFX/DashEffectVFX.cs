﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashEffectVFX : MonoBehaviour
{
	public Color color;
	public TimeUnit shadowRemainTime;
	private SpriteRenderer spriteRenderer;
	private TimeUnitCooldown cooldown;
	private float startTime;

    void Start()
    {
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.material = new Material(spriteRenderer.material);
		spriteRenderer.material.SetColor("_Color", color);

		cooldown = new TimeUnitCooldown(shadowRemainTime);
		cooldown.Begin();

		startTime = Time.time;
    }

    void Update()
    {
		var alphaColor = color;
		alphaColor.a = color.a * (1 - Easings.Linear(cooldown.Progress));
		spriteRenderer.material.SetColor("_Color", alphaColor);
		spriteRenderer.sortingOrder = -1 * Mathf.CeilToInt(cooldown.Progress * 4 + 1);
    }
}
