﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashRenderer : MonoBehaviour
{
	[SerializeField]
	private GameObject dashVFXPrefab;
	[SerializeField]
	private TimeUnit dashShadowRemainTime = TimeUnit.Beat;
	private SpriteRenderer spriteRenderer;
	private TimeUnitCooldown cooldown;
	private Vector3 lastPosition;
	public Color color;
	
    // Use this for initialization
    void Start()
    {
		spriteRenderer = transform.parent.GetComponent<SpriteRenderer>();

		if(!spriteRenderer)
		{
			Debug.LogError("Dash Renderer didn't get parent's sprite renderer, please check if there is any problem.");
			this.enabled = false;
		}

		cooldown = new TimeUnitCooldown(TimeUnit.Step * 0.25f);		
    }

    // Update is called once per frame
    void Update()
    {
		var targetPosition = spriteRenderer.transform.position;
		if(cooldown.IsFinished)
		{
			var VFXspriteRenderer = Instantiate(dashVFXPrefab, targetPosition, Quaternion.identity).GetComponent<SpriteRenderer>();
			VFXspriteRenderer.sprite = spriteRenderer.sprite;
			VFXspriteRenderer.flipX = spriteRenderer.flipX;
			VFXspriteRenderer.flipY = spriteRenderer.flipY;
			VFXspriteRenderer.sortingOrder = -1;
			var renderer = VFXspriteRenderer.GetComponent<DashEffectVFX>();
			renderer.color = color;
			renderer.shadowRemainTime = dashShadowRemainTime;
			Destroy(VFXspriteRenderer.gameObject, dashShadowRemainTime.ToTime());
			cooldown.Begin();
			lastPosition = targetPosition;
		}
    }
}
