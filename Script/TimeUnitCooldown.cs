﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeUnitCooldown
{
	public bool IsFinished {
		get
		{
			return Time.time >= lastBeginTime + CooldownTime;
		}
	}

	public float RemainCooldown {
		get {
			if(IsFinished)
				return 0;
			else
				return lastBeginTime + CooldownTime - Time.time;
		}
	}

	public float Progress {
		get {
			if(!float.IsInfinity(CooldownTime))
				return Mathf.Clamp01((Time.time - lastBeginTime) / CooldownTime);
			else
				return 0;
		}
	}

    public float ProgressTime
    {
        get
        {
            return (Time.time - lastBeginTime);
        }
    }

	public float CooldownTime {
		get {
			if (withBias)
				return timeUnit.ToTime() - JudgeController.GetSetting(bias).aheadMs / 1000f;
			else
				return timeUnit.ToTime();
		}
	}

	private TimeUnit timeUnit;
	private float lastBeginTime;
	private JudgeLevel bias;
	private bool withBias = false;
	private bool freeze = false;

	public TimeUnitCooldown(TimeUnit timeUnit)
	{
		this.timeUnit = timeUnit;
		Reset();
	}

	public TimeUnitCooldown(TimeUnit timeUnit, JudgeLevel bias) : this(timeUnit)
	{
		this.bias = bias;
		withBias = true;
	}

	public void Update()
	{
		if(freeze)
			lastBeginTime += Time.deltaTime;
	}

	public void Begin()
	{
		lastBeginTime = Time.time;
	}

	public void Reset()
	{
		lastBeginTime = -timeUnit.ToTime();
	}

	public void Reset(float remainCooldown)
	{
		lastBeginTime = Time.time - CooldownTime + remainCooldown;
	}
	
	public void Reset(TimeUnit remainCooldown)
	{		
		lastBeginTime = Time.time - CooldownTime + remainCooldown.ToTime();
	}

	public void Freeze()
	{
		freeze = true;
	}

	public void Unfreeze()
	{
		freeze = false;
	}
}
